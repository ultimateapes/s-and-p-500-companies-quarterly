import datetime as dt
import numpy as np
import pandas as pd

from glob import glob
import sys

from os import makedirs
from os.path import exists, join, splitext, basename


SAVE_PATH = 'data/output/'
READ_PATH = 'data/imputed/'

PATH_CONSTITUENTS_CHANGES = 'data/imputed/constituents_changes.csv'
PATH_CONSTITUENTS_CURRENT = 'data/imputed/constituents_current.csv'


def create_dirs():
    makedirs("data/output", exist_ok=True)

def keep_months_within_bounds(months:int, year:int):
    while months < 1:
        year = year -1
        months = months + 12
    while months > 12:
        year = year + 1
        months = months - 12
    return months, year

def extract_ints_from_datestring(date_string:str):
    year = int(date_string[:4])
    month = int(date_string[5:7])
    day = int(date_string[8:10])
    if day != 1 or month not in (1,4,7,10):
        raise ValueError('Error: Script only supports dates representative of 1st day of the quarter (eg 2021-10-01)')
    return year, month, day

def compose_datestring_from_ints(year, month, day):
    return f'{year:04d}-{month:02d}-{day:02d}'

def month_arithmatic_on_date_string(date_string:str, months:int):
    current_year, current_month, current_day = extract_ints_from_datestring(date_string)
    current_month += months
    current_month, current_year = keep_months_within_bounds(current_month, current_year)
    return compose_datestring_from_ints(current_year, current_month, current_day)

def split_s_and_p_change_single_operation(df, removal=False):
    base_columns = ['Symbol', 'Name', 'Class', 'CIK', 'CUSIP']
    columns = [column + ' (Removed)' if removal else column + ' (Added)' for column in base_columns]
    df = df[columns].dropna(subset=[columns[0]]).copy()
    df.columns = base_columns
    return df

def exclude_name_only_changes(df, evaluate_class=False):
    symbol_change_only = (df['CIK (Added)'] == df['CIK (Removed)']) & (df['CUSIP (Added)'] == df['CUSIP (Removed)'])
    if evaluate_class:
        symbol_change_only = symbol_change_only & (df['Class (Added)'] == df['Class (Removed)'])
    if symbol_change_only.any():
        print('Ignored these updates:')
        print(df[symbol_change_only])
    return df[~symbol_change_only]

def split_s_and_p_change_operations(df, start, end):
    df = df[(df['Date'] >= start) & (df['Date'] < end)]
    df = exclude_name_only_changes(df, evaluate_class=False)
    # Additions and subtractions will work in reverse because the dataframe is modified backwards through time
    additions = split_s_and_p_change_single_operation(df)
    subtractions = split_s_and_p_change_single_operation(df, removal=True)
    return additions, subtractions

def process_ranged_changes_s_and_p(df, change_df, start, end):
    additions, subtractions = split_s_and_p_change_operations(change_df, start, end)
    df = pd.concat([df.set_index('CUSIP').drop(index=additions['CUSIP']).reset_index()[subtractions.columns], subtractions]).sort_values(by=['Name'], key=lambda col: col.str.lower()).reset_index(drop=True)
    return df

def size_check_df_s_and_p(df, groupby_col:str, starting_time:str=''):
    required_rows=500
    row_count = df.groupby(groupby_col).first().shape[0]
    if row_count != required_rows:
        raise RuntimeError(f'{starting_time} S & P 500 df diverges from 500 companies. There are {row_count} rows')
        
def create_constiuents_for_each_quarter(df, change_df, current_period_start:str):
    base_path = SAVE_PATH + 'constituents_'
    datetime_format = '%Y-%m-%d'
    base_columns = ['Symbol', 'Name', 'Class', 'CIK', 'CUSIP']
    start = current_period_start
    end = month_arithmatic_on_date_string(start, 3)
    start_dt = dt.datetime.strptime(start, datetime_format)
    df = df[base_columns]
    df.to_csv(base_path + 'current.csv', index=False)
    while start_dt > dt.datetime(2017, 11, 1):
        df = process_ranged_changes_s_and_p(df, change_df, start, end)
        size_check_df_s_and_p(df, 'Name', starting_time=start)
        # Filenames need to be offset further before naming in order to indicate the last date just before the processed changes
        # to stay consistent with Fintel effective dates
        end = start
        start = month_arithmatic_on_date_string(start, -3)
        start_dt = dt.datetime.strptime(start, datetime_format) - dt.timedelta(days=1)
        save_path = base_path + dt.datetime.strftime(start_dt.date(), datetime_format) + '.csv'
        df.to_csv(save_path, index=False)
        
def create_sandp_500_reference_df(current_period_start:str, columns:str, filename_base:str):
    file_prefix = 'constituents_'
    date_path, symbol_path = [SAVE_PATH + kind + '_' + filename_base + '.csv' for kind in ['period', 'symbol']]
    datetime_format = '%Y-%m-%d'
    dfs = []
    current_period_start = dt.datetime.strptime(current_period_start, datetime_format) - dt.timedelta(days=1)
    current_period_start = dt.datetime.strftime(current_period_start.date(), datetime_format)
    for path in sorted(glob(SAVE_PATH+'/*.csv')):
        date = splitext(basename(path))[0]
        if date.startswith(file_prefix):
            date = date.replace(file_prefix, '')
        else:
            continue
        if date == 'current':
            date=current_period_start
        df = pd.read_csv(path)
        df['Date'] = pd.to_datetime(date)
        dfs.append(df)
    df = pd.concat(dfs, ignore_index=True)
    dates = df[columns + ['Date']].groupby(columns, dropna=False).agg(['min', 'max'])
    dates.columns = dates.columns.get_level_values(0) + dates.columns.get_level_values(1)
    dates.reset_index().to_csv(date_path, index=False)
    symbols = df[columns + ['Symbol']].groupby(columns, dropna=False).last()
    symbols.reset_index().to_csv(symbol_path, index=False)

def aggregate_outstanding_share_csvs(dir_path:str):
    dfs = []
    for fn in set([basename(item).rsplit('_', 1)[0] for item in glob(join(dir_path, '*.csv'))]):
        auto_path, manual_path = [join(dir_path, fn + '_' + kind + '.csv') for kind in ['auto', 'manual']]
        csv = []
        if exists(auto_path):
            csv.append(pd.read_csv(auto_path))
        if exists(manual_path):
            csv.append(pd.read_csv(manual_path))
        csv = pd.concat(csv).drop_duplicates(subset=['period', 'cik', 'class'], keep='last').sort_values(by=['class', 'period'])
        dfs.append(csv)
    return pd.concat(dfs, ignore_index=True)

def augment_shares_outstanding_to_s_and_p_csvs(current_period_start:str):
    share_dir = join(READ_PATH, 'share_counts')
    datetime_format = '%Y-%m-%d'
    file_prefix = 'constituents_'
    share_counts = aggregate_outstanding_share_csvs(share_dir)
    current_period_start = dt.datetime.strptime(current_period_start, datetime_format) - dt.timedelta(days=1)
    current_period_start = dt.datetime.strftime(current_period_start.date(), datetime_format)
    for path in sorted(glob(SAVE_PATH+'/*.csv')):
        date = splitext(basename(path))[0]
        if date.startswith(file_prefix):
            date = date.replace(file_prefix, '')
        else:
            continue
        if date == 'current':
            date=current_period_start
        df = pd.read_csv(path)
        df['Class'] = df['Class'].str.lower()
        share_counts_current = share_counts[share_counts['period'] == date].set_index(['cik', 'class']).drop(columns=['period'])
        df = df.join(share_counts_current, on=['CIK', 'Class'], how='left')
        df.to_csv(path, index=False)

def process():
    current_period_start = sys.argv[1]
    constituents_changes = pd.read_csv(PATH_CONSTITUENTS_CHANGES, dtype={'CIK (Added)':str, 'CIK (Removed)':str})
    constituents_current = pd.read_csv(PATH_CONSTITUENTS_CURRENT, dtype={'CUSIP':str})
    create_constiuents_for_each_quarter(constituents_current, constituents_changes, current_period_start)
    create_sandp_500_reference_df(current_period_start, columns=['CIK', 'Class'], filename_base='cik')
    create_sandp_500_reference_df(current_period_start, columns=['CUSIP'], filename_base='cusip')
    augment_shares_outstanding_to_s_and_p_csvs(current_period_start)

if __name__ == "__main__":
    process()
