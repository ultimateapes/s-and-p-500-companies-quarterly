import numpy as np
import pandas as pd

from os import makedirs
from os.path import exists, join


SAVE_PATH = 'data/generated/'
READ_PATH = 'data/scrapes/'

PATH_CONSTITUENTS_CHANGES = 'data/scrapes/constituents_changes.csv'
PATH_CONSTITUENTS_MISSING= 'data/scrapes/constituents_changes_missing.csv'
PATH_CONSTITUENTS_CURRENT = 'data/scrapes/constituents_current.csv'
PATH_CIK_SYMBOL_MAPPINGS = 'data/scrapes/ticker.txt'
PATH_FTDS = 'data/scrapes/ftds.csv'


FTD_SYMBOL_UPDATES = {'BFB':'BF.B','BRKB':'BRK.B',}


def create_dirs():
    makedirs("data/scrapes", exist_ok=True)

def extract_class_from_wikipedia_table(df, name_column:str, class_column:str, class_seperator:str, class_extra_chars:str=')'):
    df = df.copy()
    (_, df[name_column]), _, (_, class_series) = list(df[name_column].str.partition(class_seperator).items())
    df[name_column] = df[name_column].str.strip()
    class_series = class_series.str.replace(class_extra_chars, '', regex=False).str.strip().replace('', np.nan)
    if class_column in df.columns:
        df[class_column] = np.where(class_series.isna(), df[class_column], class_series)
    else:
        df[class_column] = class_series
    return df


def expand_constituents_changes(df):
    df = df.copy()
    df.insert(3, 'Class (Added)', np.nan)
    df.insert(4, 'CIK (Added)', np.nan)
    df.insert(5, 'CUSIP (Added)', np.nan)
    df.insert(8, 'Class (Removed)', np.nan)
    df.insert(9, 'CIK (Removed)', np.nan)
    df.insert(10, 'CUSIP (Removed)', np.nan)
    return df


def get_longest_substring_regex(string:str, separator=None, excluded_regex:list=[]):
    if len(excluded_regex) == 0:
        return max(separator.split(string), key=len) if isinstance(string, str) else string
    else:
        return max([substring for substring in separator.split(string) if not any([bool(pattern.search(substring)) for pattern in excluded_regex])], key=len) if isinstance(string, str) else string

def pandas_map_value_from_dict_keeping_defaults(value, mappings:dict):
    updated = mappings.get(value)
    if updated:
        return updated
    return value

def create_cusip_mappings_from_ftd_df(df:pd.core.frame.DataFrame):
    return df.dropna(subset=['Symbol']).sort_values(by=['SETTLEMENT DATE']).groupby(['Symbol', 'CUSIP']).last().sort_values(by=['Symbol']).reset_index().rename(columns={'SETTLEMENT DATE':'Approx Last Date'})

def create_cusip_mappings_from_ftd_df_to_csv(df:pd.core.frame.DataFrame):
    save_path = SAVE_PATH + 'ftd_cusip_mappings.csv'
    df = create_cusip_mappings_from_ftd_df(df)
    df.to_csv(save_path, index=False)
    return df

def reduce_cusip_mapping_df_to_most_recent(df:pd.core.frame.DataFrame):
    return df.sort_values(by=['Approx Last Date']).groupby('Symbol').last().reset_index()

def merge_cusip_to_constituents(base_df:pd.core.frame.DataFrame, cusip_mappings:pd.core.frame.DataFrame, symbol_mappings:dict,):
    cusip_mappings = reduce_cusip_mapping_df_to_most_recent(cusip_mappings)
    cusip_mappings['Symbol'] = cusip_mappings['Symbol'].map(lambda x: pandas_map_value_from_dict_keeping_defaults(x, symbol_mappings))
    return base_df.merge(cusip_mappings, how='left', on='Symbol').rename(columns={'Approx Last Date':'CUSIP Updated To'})

def updated_constituents_changes_csv(df):
    save_path = SAVE_PATH + 'constituents_changes.csv'
    
    df = expand_constituents_changes(df)
    df['Reason'] = df['Reason'].str.replace(r'\W+', ' ', regex=True)
    df.to_csv(save_path, index=False)
    return df

def updated_constituents_current_csv(df:pd.core.frame.DataFrame, cusip_mappings:pd.core.frame.DataFrame):
    save_path = SAVE_PATH + 'constituents_current.csv'
    df = extract_class_from_wikipedia_table(df, name_column='Name', class_column='Class', class_seperator='(Class')
    df = extract_class_from_wikipedia_table(df, name_column='Name', class_column='Class', class_seperator='(Series')
    df = merge_cusip_to_constituents(df, cusip_mappings, FTD_SYMBOL_UPDATES)
    df.to_csv(save_path, index=False)
    return df

def process():
    constituents_changes = pd.read_csv(PATH_CONSTITUENTS_CHANGES)
    constituents_current = pd.read_csv(PATH_CONSTITUENTS_CURRENT)
    ftds = pd.read_csv(PATH_FTDS, usecols=['SYMBOL', 'CUSIP', 'DESCRIPTION', 'SETTLEMENT DATE'], parse_dates=['SETTLEMENT DATE'], dtype={'CUSIP':str}).rename(columns={'SYMBOL':'Symbol'})
    
    cusip_mappings = create_cusip_mappings_from_ftd_df_to_csv(ftds)
    constituents_changes = updated_constituents_changes_csv(constituents_changes)
    constituents_current = updated_constituents_current_csv(constituents_current, cusip_mappings)

if __name__ == "__main__":
    process()
