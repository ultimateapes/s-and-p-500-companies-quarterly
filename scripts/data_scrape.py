from bs4 import BeautifulSoup
from datetime import datetime
from shutil import copyfileobj
import csv
from os import makedirs
from os.path import exists, join
import urllib.request as request


SOURCE = "https://en.wikipedia.org/wiki/List_of_S%26P_500_companies"
CIK_SOURCE = "https://www.sec.gov/include/ticker.txt"
CACHE = join("scripts/tmp", "List_of_S%26P_500_companies.html")

def create_dirs():
    makedirs("data/scrapes", exist_ok=True)
    makedirs("scripts/tmp", exist_ok=True)

def retrieve():
    request.urlretrieve(SOURCE, CACHE)

def write_csv(file, header, records, table_id):
    with open(file, "w") as f:
        writer = csv.writer(f, lineterminator="\n")
        writer.writerow(header)
        # Sorting ensure easy tracking of modifications
        if table_id == "constituents":
            records.sort(key=lambda s: s[1].lower())
        elif table_id == "changes":
            records.sort(key=lambda s: s[0], reverse=True)
        writer.writerows(records)

def extract_current_constituents(soup):
    table_id = "constituents"
    filename = "./data/scrapes/constituents_current.csv"
    table = soup.find("table", {"id": table_id})

    # Fail now if we haven't found the right table
    header = table.findAll("th")
    if header[0].text.rstrip() != "Symbol" or header[1].string != "Security":
        raise Exception("Can't parse Wikipedia's table!")

    # Retrieve the values in the table
    records = []
    symbols = []
    rows = table.findAll("tr")
    for row in rows:
        fields = row.findAll("td")
        if fields:
            symbol = fields[0].text.rstrip()
            # fix as now they have links to the companies on WP
            name = fields[1].text.replace(",", "")
            sector = fields[3].text.rstrip()
            cik = fields[7].text.rstrip()
            records.append([symbol, name, cik, sector])
            symbols.append(symbol + "\n")

    header = ["Symbol", "Name", "CIK", "Sector"]
    write_csv(filename, header, records, table_id)

def extract_constituents_changes(soup):
    table_id = "changes"
    filename = "./data/scrapes/constituents_changes.csv"
    table = soup.find("table", {"id": table_id})

    # Fail now if we haven't found the right table
    header = table.findAll("th")

    if header[0].text.rstrip() != "Date" or header[1].text.rstrip() != "Added":
        raise Exception("Can't parse Wikipedia's table!")
    
    records = []
    rows = table.findAll("tr")
    for row in rows:
        fields = row.findAll("td")
        if fields:
            date = datetime.strptime(fields[0].text.rstrip(), "%B %d, %Y")
            symbol_addition = fields[1].text.rstrip()
            name_addition = fields[2].text.rstrip()
            symbol_removal = fields[3].text.rstrip()
            name_removal = fields[4].text.rstrip()
            reason = fields[5].text.rstrip()
            records.append([date, symbol_addition, name_addition, symbol_removal, name_removal, reason])
    header = ["Date", "Symbol (Added)", "Name (Added)", "Symbol (Removed)", "Name (Removed)", "Reason"]
    write_csv(filename, header, records, table_id)

def cik_download():
    filename = "./data/ticker.txt"
    with request.urlopen(CIK_SOURCE) as response, open(filename, 'wb') as out_file:
        copyfileobj(response, out_file)
    
def extract():
    source_page = open(CACHE).read()
    soup = BeautifulSoup(source_page, "html.parser")
    extract_current_constituents(soup)
    extract_constituents_changes(soup)
    
    #with open("./data/constituents_symbols.txt", "w") as f:
        # Sorting ensure easy tracking of modifications
        #symbols.sort(key=lambda s: s[0].lower())
        #f.writelines(symbols)


def process():
    create_dirs()
    retrieve()
    extract()
    cik_download()


if __name__ == "__main__":
    process()
