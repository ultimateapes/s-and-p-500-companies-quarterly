These **Linux** scripts scrape data from Wikipedia page about S&P500. They have additional dependencies from the upstream repo since they facilitate merging CUSIP numbers that can be found in SEC failure to deliver data and allow the creation of various snapshots of the S & P 500 over time.

# Run the scripts

## Install the dependencies

The scripts work with some python and shell scripts glued together with a Makefile.

Install the required python libraries :

    cd scripts
    pip install -r requirements.txt

You can also work on a [virtualenv](http://docs.python-guide.org/en/latest/dev/virtualenvs/) .

## Scrape from wikipedia

Running [data_scrape.py](data_scrape.py) will output two csvs to the [data/scrapes](../data/scrapes/) directory. One is a table containing the current S & P constituents and the other is Wikipedia's (incomplete) record of changes to the index over time.

## Merge the scraped data to generate csvs for imputation

Running [data_merge.py](data_merge.py) will create an additional directory, [data/generated](../data/generated/), which will contain additional blank columns for imputation in the s & p change csv and the current index with merged CUSIP numbers (taken from the [SEC failure to deliver records](https://www.sec.gov/data/foiadocsfailsdatahtm))

### Remember to add in Failure to Deliver Data from the SEC
The scripts won't run without the FTD data saved as "ftds.csv" in the (../data/scrapes/) directory. A complete set of failure to deliver data can be downloaded and merged with the correct format using scripts contained in [this repository](https://gitlab.com/smoothcodemonkeys/themayonnaise).
This will also create a reduced csv of the FTD data that functions as a more convenient reference than the raw data.

## Impute missing values
With a constantly changing index and many mergers and aquisitions over time, the CUSIP and CIK fields can only be reliably automated for current listings. The [data/imputed](../data/imputed/) folder contains this information painstakenly referenced manually for accuracy. It is an incomplete work in progress, but aims to be filled in over time starting with present data and working backwards. Additional contributers would be extremely helpful to assist in this research.

### Warning
I've encountered bugs when updating [constituents_changes.csv](../data/imputed/constituents_changes.csv) where the CUSIP column will experience data loss upon opening. Some of the ids are incorrectly interpreted as floating point values with scientific notation under rare circumstances. For that reason the [libreoffice_non_destructive_csv_import.txt](libreoffice_non_destructive_csv_import.txt) macro has been included. Running it will prompt to open the file, but it will be parsed as text only to prevent any incidental loss. Credit for the macro goes to [this thread](https://ask.libreoffice.org/t/calc-read-write-csv-without-any-conversion/65080/11).
